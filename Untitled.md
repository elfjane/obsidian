

book
===
aaa
---

# 123
###### 副標題

> markdown 教學。11111111111111111111111111111111111111111111111111111111111111111111111111111111111233
> 學習 markdown 語法

* 要製作的
> 說明說明說 [巴哈姆特](https://acg.gamer.com.tw/search.php?encode=utf8&kw=LIVE+A+LIVE)明說明說明說明說明說明說明說明說明說明說明說明說明
> > > > 123
> [說明](https://acg.gamer.com.tw/search.php?encode=utf8&kw=LIVE+A+LIVE)
* 要製作

|**Name** |**Quantity**|
|-----|--------|
|Apple|33ewrewrwerwerewrewr|
|Eggant sdfdsfdsfdsfdsfsd|12|

:::success
寄信給我 : <elfjane@gmail.com>


```javascript=
var s = "JavaScript syntax highlighting";
alert(s);
function $initHighlight(block, cls) {
  try {
    if (cls.search(/\bno\-highlight\b/) != -1)
      return process(block, true, 0x0F) +
             ' class=""';
  } catch (e) {
    /* handle exception */
  }
  for (var i = 0 / 2; i < classes.length; i++) {
    if (checkCondition(classes[i]) === undefined)
      return /\d+[\s/]/g;
  }
}
```

```javascript=
var s = "ng";
alert(s);
function $initHighlight(block, cls) {
 var a=3;
 console.log(a);
}
```
* 要製作的二

+要製作的
+ 要製作
+ 要製作的二
* 測試

1. [ ] test
2. [x] t est2
3. test3
***

+ Item1
    + Item1.1
    + Item1.2
- Item2
    - Item2.1
    - Item2.2
        * Item2.2.1
1. Number
2. Number
2. Number
- [x] CheckBox
+ [x] To-Do
* [ ] To-Do

***
使用`語`法
aaaa
    bbbb
    dfsfdf
        ddsfdsf
            asdfdffd
```

# 123
###### 副標題

> markdown 教學。1111111111111111`11111111111`11111111111111111111111111111111111111111111111111111111233
> 學習 markdown 語法

***
```
# This is an H1
## This is an H2

###### This is an H6


# This is an H1 #

## This is an H2 ##

### This is an H3 ######


> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.

> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse.  id sem consectetuer libero luctus adipiscing.



* * *

***

*****

- - -

---------------------------------------


[Google]: http://google.com/


I get 10 times more traffic from [Google] [1] than from
[Yahoo] [2] or [MSN] [3].

  [1]: http://google.com/        "Google"
  [2]: http://search.yahoo.com/  "Yahoo Search"
  [3]: http://search.msn.com/    "MSN Search"


如果改成用連結名稱的方式寫：


I get 10 times more traffic from [Google][] than from
[Yahoo][] or [MSN][].

  [google]: http://google.com/ "Google"
  [yahoo]:  http://search.yahoo.com/  "Yahoo Search"
  [msn]:    http://search.msn.com/    "MSN Search"


<p>I get 10 times more traffic from <a href="http://google.com/"
title="Google">Google</a> than from
<a href="http://search.yahoo.com/" title="Yahoo Search">Yahoo</a>
or <a href="http://search.msn.com/" title="MSN Search">MSN</a>.</p>


I get 10 times more traffic from [Google](http://google.com/ "Google") than from [Yahoo](http://search.yahoo.com/ "Yahoo Search") or [MSN](http://search.msn.com/ "MSN Search").



### Table - Text

1 | 2 | 3
--- | --- | ---
one | two | three
*Italic* | `Hightlight` | **Bold**

### Table - Mix

|Table 1|Table 2| 
|--|--| 
|<table> <tr><th>Table 1 Heading 1</th><th>Table 1 Heading 2</th></tr><tr><td>Row 1 Column 1</td><td>Row 1 Column 2</td></tr> </table>| <table> <tr><th>Table 2 Heading 1</th><th>Table 2 Heading 2</th></tr><tr><td>Row 1 Column 1</td><td>Row 1 Column 2</td></tr> </table>| 


### Html - Item


#### 我要畫表格


|姓名|數學|英文|1234567891234564879|
|--|--|--|--|
|劉德華|100|95|1. test|
|爸爸|100|95|1123|
|123|


